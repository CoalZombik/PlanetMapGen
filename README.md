# PlanetMap Generator
Application for convert 1:1 texture to equirectangular projection (2:1) texture (https://en.wikipedia.org/wiki/Equirectangular_projection). <br>
Written in C#. Works in all computers with .NET (https://www.microsoft.com/net) or Mono (http://www.mono-project.com/). <br>

## Usage
Download PlanetMapGen.exe or build from source (Program.cs). <br>
Run this app in terminal `PlanetMapGen.exe` (resp. `mono PlanetMapGen.exe`) or run with args `PlanetMapGen.exe [input file] [output file]`.
