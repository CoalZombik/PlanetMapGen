﻿// PlanetMap Generator; (c) Coal Games (Lukáš Cezner) in Apache-2.0
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace PlanetMapGen
{
	class MainClass
	{
		static int InputX, InputY;
		static int OutputX, OutputY;

		static string InputFile = "input.png", OutputFile = "output.png";

		public static void Main (string[] args)
		{
			Console.OutputEncoding = Encoding.UTF8;
			Console.InputEncoding = Encoding.UTF8;

			Console.WriteLine ("PlanetMap Generator; (c) Coal Games (Lukáš Cezner) in Apache-2.0");
			Console.WriteLine ("================================================================");
			if (args.Length == 2)
			{
				InputFile = args[0];
				OutputFile = args[1];
			}
			else
			{
				Console.Write ("Input file: ");
				InputFile = Console.ReadLine ();
				Console.Write ("Output file: ");
				OutputFile = Console.ReadLine ();
			}

			if (!File.Exists (InputFile))
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine ("Input file not found (" + InputFile + ").");
				Console.ResetColor ();
				return;
			}

			Bitmap Input = new Bitmap (InputFile);
			InputX = Input.Width;
			OutputX = InputX;
			InputY = Input.Height;
			OutputY = InputY / 2;

			Console.WriteLine ("Input: " + InputFile + " (" + InputX + ", " + InputY + ")");
			Console.WriteLine ("Output: " + OutputFile + " (" + OutputX + ", " + OutputY + ")\n");
			Console.ForegroundColor = ConsoleColor.Red;
			DateTime StartTime = DateTime.Now;

			Bitmap Output = new Bitmap (OutputX, OutputY, PixelFormat.Format32bppArgb);

			Barvy[,] NoveBarvy = new Barvy[OutputX, OutputY];

			for (int y = 0; y < InputY; y++)
			{
				int NovaY = y - InputY / 2;
				NovaY = (int) ((OutputY + Math.Sin (NovaY * Math.PI / InputY) * OutputY) / 2);

				for (int x = 0; x < InputX; x++)
				{
					NoveBarvy[x, NovaY].Add (Input.GetPixel (x, y));
				}
			}

			for (int y = 0; y < OutputY; y++)
			{
				for (int x = 0; x < OutputX; x++)
				{
					Output.SetPixel (x, y, NoveBarvy[x, y].Prumer ());
				}
			}

			TimeSpan Time = DateTime.Now - StartTime;
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine ("Done! " + Time.TotalMilliseconds + " ms; " + (float) ((InputX * InputY) / Time.TotalSeconds) + " px/s");
			Output.Save (OutputFile, ImageFormat.Png);
			Console.ResetColor ();
		}

		struct Barvy
		{
			public List<Color> VsechnyBarvy;

			public void Add (Color JednaBarva)
			{
				if (VsechnyBarvy == null)
					VsechnyBarvy = new List<Color> ();
				VsechnyBarvy.Add (JednaBarva);
			}

			public Color Prumer ()
			{
				int PocetBarev = VsechnyBarvy.Count;

				if (PocetBarev < 1)
					return Color.FromArgb (0, 0, 0, 0);

				int R = 0, G = 0, B = 0;
				foreach (Color JednaBarva in VsechnyBarvy)
				{
					R += JednaBarva.R;
					G += JednaBarva.G;
					B += JednaBarva.B;
				}

				return Color.FromArgb (255, R / PocetBarev, G / PocetBarev, B / PocetBarev);
			}
		}
	}
}